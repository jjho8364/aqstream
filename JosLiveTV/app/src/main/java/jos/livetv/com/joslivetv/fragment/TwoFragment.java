package jos.livetv.com.joslivetv.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jos.livetv.com.joslivetv.R;


/**
 * Created by leegs on 2016-06-25.
 */
public class TwoFragment extends Fragment implements View.OnClickListener {

    public TwoFragment(){

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment02, container, false);

        return view;
    }

    @Override
    public void onClick(View view) {

    }
}
